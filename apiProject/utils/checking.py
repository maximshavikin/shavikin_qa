"""Методы для проверки ответов наших запросов"""
import json

from requests import Response


class Checking():

    """Метод для проверки статус кода"""
    @staticmethod
    def check_status_code(response: Response,status_code):
        assert status_code == response.status_code
        if response.status_code == status_code:
            print("Успех! Статус код = " + str(response.status_code))
        else:
            print("Провал! Статус код = " + str(response.status_code))


    """Метод для проверки полей в ответе запроса"""
    @staticmethod
    def check_json_token(response: Response, expected_value):

        token = json.loads(response.text)
        assert list(token) == expected_value
        print('Все поля присутствуют')

    """Метод для проверки значений обязательных полей в ответе запроса"""
    @staticmethod
    def check_json_value(response: Response, filed_name, expected_value):
        check = response.json()
        check_info = check.get(filed_name)
        assert check_info == expected_value
        print(filed_name + " - Верно")

    @staticmethod
    def check_json_search_word(response: Response, filed_name, search_word):
        check = response.json()
        check_info = check.get(filed_name)
        if search_word in check_info:
            print("Слово - " + search_word + " присутствует")

